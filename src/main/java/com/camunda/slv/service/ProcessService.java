/**
 * 
 */
package com.camunda.slv.service;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.camunda.slv.model.ResponseEnvelope;
import com.camunda.slv.model.UserDetails;
import com.camunda.slv.repo.UserRepo;

/**
 * @author selvark
 *
 */
@Service
public class ProcessService implements IProcessService {
	private static final Logger LOGGER = LoggerFactory.getLogger(ProcessService.class);
	
	@Autowired
	UserRepo userRepo;

	@Override
	public ResponseEnvelope checkOnboardingStatus(UserDetails userInfo) {
		ResponseEnvelope response = new ResponseEnvelope();

		return response;

	}

	@Override
	public ResponseEnvelope checkOnboardingStatus(String firstName, String familyName, String dob) {
		ResponseEnvelope response = new ResponseEnvelope();
		Map<String, Object> processVariables = new HashMap<>();
		processVariables.put("firstName", firstName);
		processVariables.put("familyName", familyName);
		processVariables.put("dob", dob);
		//camundaService.startProcessByMessage(processVariables);

		// runtimeService.startProcessInstanceByKey("someKey", processVariables);
		// runtimeService.activateProcessInstanceById("slv-flow-process");
		// runtimeService().startProcessInstanceByKey("slv-flow-process",
		// withVariables("firstName", "selva","familyName", "kannaiyan","dob",
		// "26042020"));
		return response;
	}

}
