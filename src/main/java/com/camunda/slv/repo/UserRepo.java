/**
 * 
 */
package com.camunda.slv.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.camunda.slv.model.UserEntity;

/**
 * @author selvark
 *
 */

@Repository
public interface UserRepo extends JpaRepository<UserEntity, Long> {

	//Optional<UserEntity> findByfirstNameAndfamilyNameAndDOB(String firstName, String familyName, String dob);

	//UserEntity findAllByfirstNameAndfamilyNameAndDOB(String firstName, String familyName, String dob);

}