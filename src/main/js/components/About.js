import React from "react"
import {Button, Container} from "reactstrap"
import {Link, withRouter} from "react-router-dom"


class About extends React.Component {
  render() {
    return (
      <Container>
        <h2>React.js and Spring Data REST - CRUD</h2>
        <p>
          CRUD application with security enabled: a PoC with
          ReactJS in the frontend and Spring Data REST in the backend.
        </p>
       
        <p>
          <strong>Source Code</strong>:&nbsp;
          <a href="https://gitlab.com/bnp-camunda-poc/sample-project"
             target="_blank" rel="noopener noreferrer">
           https://gitlab.com/bnp-camunda-poc/sample-project
          </a>
        </p>
        
        <Button color="link"><Link to="/">← Back to Users</Link></Button>
      </Container>
    )
  }
}

export default withRouter(About)
