/**
 * 
 */
package com.camunda.slv.model;



import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


import javax.persistence.*;


/**
 * @author selvark
 *
 */

@Entity
@Table(name = "userentity")
@Getter
@Setter
@AllArgsConstructor
@ToString
public class UserEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "firstName")
	public String firstName;

	@Column(name = "familyName")
	public String familyName;
	
	@Column(name = "dob")
	public String dob;

}