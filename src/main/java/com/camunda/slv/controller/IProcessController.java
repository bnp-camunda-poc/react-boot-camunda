/**
 * 
 */
package com.camunda.slv.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.camunda.slv.model.ResponseEnvelope;

/**
 * @author selvark
 *
 */

@RequestMapping("/v1/slvprocess")
public interface IProcessController {
	@CrossOrigin
	@GetMapping(value = "/status")
	public ResponseEnvelope slvProcess(@RequestParam String firstName, @RequestParam String familyName, @RequestParam String dob);

}
