/**
 * 
 */
package com.camunda.slv.delegate;

import java.util.Random;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author selvark
 *
 */

@Component
public class UserDelegate implements JavaDelegate {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserDelegate.class);

	@Override
	public void execute(DelegateExecution execution) throws Exception {
		execution.setVariable("found", new Random().nextBoolean());
		LOGGER.info("Delegate executed ");
	}

}
