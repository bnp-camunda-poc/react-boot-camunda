/**
 * 
 */
package com.camunda.slv.service;

import com.camunda.slv.model.ResponseEnvelope;
import com.camunda.slv.model.UserDetails;

/**
 * @author selvark
 *
 */
public interface IProcessService {
	ResponseEnvelope checkOnboardingStatus(UserDetails userInfo);

	ResponseEnvelope checkOnboardingStatus(String firstName, String familyName, String dob);
}
