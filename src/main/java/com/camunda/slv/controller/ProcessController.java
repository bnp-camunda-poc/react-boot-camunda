/**
 * 
 */
package com.camunda.slv.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.camunda.slv.model.ResponseEnvelope;
import com.camunda.slv.service.IProcessService;

/**
 * @author selvark
 *
 */

@RestController
public class ProcessController implements IProcessController{

	@Autowired
	IProcessService processService;
	
	@Override
	public ResponseEnvelope slvProcess(String firstName, String familyName, String dob) {
		// TODO Auto-generated method stub
		return processService.checkOnboardingStatus( firstName, familyName, dob);
	}

}
