ReactJs + Spring Boot with Camunda Webapps for SLV flow process

What we developed in this initial pacakge:

Embedded Camunda engine
Camunda webapp packs automatically deployed
Process application and any number of BPMN process can be deployed ( according to the slv flow process business use case, we have to modify or add new BPMN process )
Admin user configured with login and password configured in application.yaml


We have put BPMN, CMMN and DMN files in our classpath, it will be automatically deployed and registered within a process application.
Run the application and use Camunda Webapps

Requirements
JDK 8+
Maven 3+, or you can use the script ./mvnw instead that it will install Maven 3.6 in the user space automatically if the required version isn't there
To build the web assets the project uses Node.js, Webpack, ... but all of them are installed and triggered by Maven automatically in the user space.

Usage
Launch the application with:

$ mvn spring-boot:run

Packaging
Pack the application in a single .jar with all the dependencies and the web server with:

$ mvn package
Then you can run the .jar with:

$ java -jar target/app-0.0.1-SNAPSHOT.jar
Tests
For now only a test that checks that the spring context can be loaded is in the source code. Execute with:

$ mvn test

Hot reloading
To edit Javascript or CSS resources and see the changes in the browser without the need to re-launch the application, execute within a command line:

$ npm run watch

Then we can access Camunda Webapps in browser: http://localhost:8081 (provide login/password from application.yaml, default: demo/demo)